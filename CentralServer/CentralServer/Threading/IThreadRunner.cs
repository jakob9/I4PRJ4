﻿using System;

namespace CentralServer.Threading
{
    public interface IThreadRunner
    {
        void RunThread();
    }
}
