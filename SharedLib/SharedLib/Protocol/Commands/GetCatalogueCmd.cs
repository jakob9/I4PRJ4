﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharedLib.Protocol.Commands
{
    /// <summary>
    /// Covers the need for a "send me the catalogue" command.
    /// </summary>
    public class GetCatalogueCmd: Command
    {
    }
}
